/// <reference types="cypress" />

describe('Formulario de cadastro de usuário.', () => {
    beforeEach('Acessa Url', () => {
        cy.visit('/index.php?controller=authentication#account-creation');
        cy.adicionaEmailParaNovaConta('emailnaocadastrado99@email.com');
    });

    it('Validar página de cadastro.', () => {
        cy.get('h1.page-heading').should('be.visible').and('contain', 'Create an account');
        cy.title().should('eq', 'Login - My Store')
    })

    it('Validar cadastro de usuário com dados válidos.', () => {
        cy.get('#id_gender1').should('be.visible').and('be.not.checked').check().should('be.checked');
        cy.preencheCamposObrigatoriosCadastroUsuario('jaspion_tokusatsu23@email.com');
        cy.get('#days').select('7').should('have.value', '7');
        cy.get('#months').select('February').should('have.value', '2');
        cy.get('#years').select('1991').should('have.value', '1991');
        cy.get('#newsletter').should('be.not.checked').check().should('be.checked');
        cy.get('#optin').should('be.not.checked').check().should('be.checked');
        cy.get('#firstname').should('be.visible').and('have.value', 'Jaspion');
        cy.get('#lastname').should('be.visible').and('have.value', 'Tokusatsu');
        cy.get('#company').should('be.visible').type('Sato Company').should('have.value', 'Sato Company');
        cy.get('#address2').should('be.visible').type('Apartamento').should('have.value', 'Apartamento');
        cy.get('textarea[id=other]').should('be.visible').type('Por favor, apertar a companhia.').should('have.value', 'Por favor, apertar a companhia.')
        cy.get('#phone').should('be.visible').type('99988776655').should('have.value', '99988776655');
        cy.get('#alias').should('be.visible').and('have.value', 'My address').clear().type('Rua Canadá. Nº 0002');
        cy.get('#submitAccount').should('be.visible').and('be.enabled').click();
        cy.title().should('eq', 'My account - My Store');
        cy.get('h1.page-heading').should('be.visible').and('contain', 'My account');
        cy.fazLogout();
    });

    it('Validar cadastro usuario com um email ja cadastrado.', () => {
        cy.preencheCamposObrigatoriosCadastroUsuario('felipe.accyole@email.com');
        cy.get('#submitAccount').should('be.visible').and('be.enabled').click();
        cy.get('h1.page-heading').should('be.visible').and('contain', 'Create an account');
        cy.get('div.alert-danger').should('be.visible').and('contain', 'An account using this email address has already been registered.');
    });

    it('Validar cadastro usuario com todos os campos obrigatorios vazios.', () => {
        cy.get('#customer_firstname').clear().should('be.empty');
        cy.get('#customer_lastname').clear().should('be.empty');
        cy.get('#email').clear().should('be.empty');
        cy.get('#passwd').clear().should('be.empty');
        cy.get('#address1').clear().should('be.empty');
        cy.get('#city').clear().should('be.empty');
        cy.get('#id_country').select('-').should('have.value', '');
        cy.get('#phone_mobile').clear().should('be.empty');
        cy.get('#alias').clear().should('be.empty');
        cy.get('#submitAccount').should('be.visible').and('be.enabled').click();
        cy.get('div.alert-danger').should('be.visible').and('contain', 'There are 11 errors');
    });

    it('Validar existencia dos campos "State" e "Zip/Postal Code" sem País válido selecionado', () => {
        cy.get('#id_country').select('-').should('have.value', '');
        cy.get('#id_state').should('not.be.visible');
        cy.get('#postcode').should('not.be.visible');
    });


});