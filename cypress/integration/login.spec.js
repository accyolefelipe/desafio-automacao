/// <reference types="cypress" />

describe('Formulario de login.', () => {
    beforeEach('Acessa Url', () => {
        cy.visit('/index.php?controller=authentication&back=my-account');
    });

    it('Validar página de login.', () => {
        cy.title().should('eq', 'Login - My Store');
        cy.get('h1.page-heading').should('be.visible').and('contain', 'Authentication');
    })

    it('Validar login de usuário com dados válidos.', () => {
        cy.get('#email').should('be.visible').type('felipe.accyole@email.com').should('have.value', 'felipe.accyole@email.com');
        cy.get('#passwd').should('be.visible').type('felipe1991').should('have.value', 'felipe1991')
        cy.get('#SubmitLogin').should('be.visible').click();
        cy.title().should('eq', 'My account - My Store');
        cy.get('h1.page-heading').should('be.visible').and('contain', 'My account');
    });

    it('Validar login de usuário com o campo Password incorreto.', () => {
        cy.get('#email').should('be.visible').type('felipe.accyole@email.com').should('have.value', 'felipe.accyole@email.com');
        cy.get('#passwd').should('be.visible').type('123').should('have.value', '123')
        cy.get('#SubmitLogin').should('be.visible').click();
        cy.get('div.alert-danger').should('be.visible').and('contain', 'Invalid password.');
    });

    it('Validar login de usuário com o campo Username vazio.', () => {
        cy.get('#passwd').should('be.visible').type('felipe1991').should('have.value', 'felipe1991')
        cy.get('#SubmitLogin').should('be.visible').click();
        cy.get('div.alert-danger').should('be.visible').and('contain', 'An email address required.');
    });

    it('Validar login de usuário com o campo Password vazio.', () => {
        cy.get('#email').should('be.visible').type('felipe.accyole@email.com').should('have.value', 'felipe.accyole@email.com');
        cy.get('#SubmitLogin').should('be.visible').click();
        cy.get('div.alert-danger').should('be.visible').and('contain', 'Password is required.');
    });

    it('Validar link de "Forgot your password?"', () => {
        cy.get('p.lost_password > a').should('be.visible').click();
        cy.title().should('eq', 'Forgot your password - My Store');
        cy.get('h1.page-subheading').should('be.visible').and('contain', 'Forgot your password?');
    });

    it('Validar opção de criar uma conta"', () => {
        cy.get('#email_create').should('be.visible').type('emailparacontanova9123@email.com').should('have.value', 'emailparacontanova9123@email.com');
        cy.get('#SubmitCreate').should('be.visible').click();
        cy.title().should('eq', 'Login - My Store');
        cy.get('h1.page-heading').should('contain', 'Create an account');
    });

    it('Validar opção de criar uma conta com e-mail ja cadastrado"', () => {
        cy.get('#email_create').should('be.visible').type('felipe.accyole@email.com').should('have.value', 'felipe.accyole@email.com');
        cy.get('#SubmitCreate').should('be.visible').click();
        cy.get('#create_account_error').should('be.visible')
            .and('contain', 'An account using this email address has already been registered. Please enter a valid password or request a new one.');
    });

});