/// <reference types="cypress" />

describe('Pagina de Vestidos Casuais', () => {
    beforeEach('', ()=> {
        cy.visit('/index.php?id_category=9&controller=category');
    });

    it('Validar Pagina de Vestidos Casuais.', () => {
        cy.title().should('eq', 'Casual Dresses - My Store');
        cy.get('h1.page-heading').should('be.visible').and('contain', 'Casual Dresses');
    });

    it('Validar Compra de Vestido Casual Após Login.', () => {
        cy.loginPeloBotaoSignIn();
        cy.acessaCategoriaVestidosCasuais();
        cy.adicionaProdutoAoCarrinho();
        cy.acessaEVerificaPaginaResumoDoCarrinhoDeCompras();
        cy.contains('.cart_navigation > a', 'Proceed to checkout').should('be.visible')
            .and('contain', 'Proceed to checkout').click();
        cy.verificaPaginaDeEnderecosEProcessa();
        cy.verificaPaginaDeFreteCheckaOsTermosEProcessa();
        cy.verificaPaginaDePagamentoEEscolheForma();
        cy.verificaPaginaDePedidoEConfirmaPedido();
        cy.get('.box > .cheque-indent').should('be.visible').and('contain', 'Your order on My Store is complete.');
    });

    it('Validar Compra de Vestido Casual Efetuando Login durante o processo de compra.', () => {
        cy.adicionaProdutoAoCarrinho();
        cy.acessaEVerificaPaginaResumoDoCarrinhoDeCompras();
        cy.contains('.cart_navigation > a', 'Proceed to checkout').should('be.visible')
            .and('contain', 'Proceed to checkout').click();
        cy.preencheEEnviaOsCamposDeLogin();
        cy.verificaPaginaDeEnderecosEProcessa();
        cy.verificaPaginaDeFreteCheckaOsTermosEProcessa();
        cy.verificaPaginaDePagamentoEEscolheForma();
        cy.verificaPaginaDePedidoEConfirmaPedido();
        cy.get('.box > .cheque-indent').should('be.visible').and('contain', 'Your order on My Store is complete.');
    });

    it('Validar Compra de Vestido Casual sem concordar com os termos de serviço de frete.', () => {
        cy.adicionaProdutoAoCarrinho();
        cy.acessaEVerificaPaginaResumoDoCarrinhoDeCompras();
        cy.contains('.cart_navigation > a', 'Proceed to checkout').should('be.visible')
            .and('contain', 'Proceed to checkout').click();
        cy.preencheEEnviaOsCamposDeLogin();
        cy.verificaPaginaDeEnderecosEProcessa();
        cy.get('#cgv').should('be.not.checked');
        cy.contains('button[name="processCarrier"]', 'Proceed to checkout').should('be.visible')
        .and('contain', 'Proceed to checkout').click();
        cy.get('.fancybox-error').should('be.visible').and('contain', 'You must agree to the terms of service before continuing.');
    });

    it('Validar limpeza de carrinho de compras.', () => {
        cy.adicionaProdutoAoCarrinho();
        cy.acessaEVerificaPaginaResumoDoCarrinhoDeCompras();
        cy.get('tbody > tr > .cart_delete').should('be.visible').click();
        cy.get('p.alert-warning').should('be.visible').and('contain', 'Your shopping cart is empty.')
    });

    it('Validar mais informações do produto.', () => {
        cy.get('.button-container > a.lnk_view').should('be.visible').click();
        cy.contains('h3', 'More info').should('be.visible').and('contain', 'More info');
    });

    it('Validar adicionar produto a lista de desejos.', () => {
        cy.loginPeloBotaoSignIn();
        cy.acessaCategoriaVestidosCasuais();
        cy.get('.wishlist > a').should('be.visible').click();
        cy.get('.fancybox-error').should('be.visible').and('contain', 'Added to your wishlist.');
    });

    it('Validar adicionar produto a lista de desejos sem estar logado.', () => {
        cy.get('.wishlist > a').should('be.visible').click();
        cy.get('.fancybox-error').should('be.visible').and('contain', 'You must be logged in to manage your wishlist.');
    });

});