Cypress.Commands.add('adicionaEmailParaNovaConta', (email) => {
    cy.get('#email_create').type(email);
    cy.intercept('POST', 'http://automationpractice.com/index.php').as('emailParaNovaConta')
    cy.get('#SubmitCreate').click();
    cy.wait('@emailParaNovaConta').its('response.statusCode').should('eq', 200);
});


Cypress.Commands.add('preencheCamposObrigatoriosCadastroUsuario', (email) => {
    cy.get('#customer_firstname').should('be.visible').type('Jaspion').should('have.value', 'Jaspion');
    cy.get('#customer_lastname').should('be.visible').type('Tokusatsu').should('have.value', 'Tokusatsu');
    cy.get('#email').should('be.visible').click().clear().should('be.empty').type(email).should('have.value', email);
        cy.get('#passwd').should('be.visible').type('jasp123').should('have.value', 'jasp123');
    cy.get('#address1').should('be.visible').type('Rua Japão. Nº 0001').should('have.value', 'Rua Japão. Nº 0001');
    cy.get('#city').should('be.visible').type('Fresno').should('have.value', 'Fresno');
    cy.get('#id_state').select('California').should('have.value', '5');
    cy.get('#postcode').should('be.visible').type('12345').should('have.value', '12345');
    cy.get('#id_country').select('United States').should('have.value', '21');
    cy.get('#phone_mobile').should('be.visible').type('99988776644').should('have.value', '99988776644');
});


Cypress.Commands.add('loginPeloBotaoSignIn', () => {
        cy.get('a.login').should('be.visible').and('contain', 'Sign in').click();
        cy.preencheEEnviaOsCamposDeLogin();
});

Cypress.Commands.add('preencheEEnviaOsCamposDeLogin', (
    email = Cypress.env('USER_EMAIL'),
    password = Cypress.env('USER_PASSWORD')) => {
        cy.get('#email').should('be.visible').type(email);
        cy.get('#passwd').should('be.visible').type(password);
        cy.get('#SubmitLogin').should('be.visible').click();
});

Cypress.Commands.add('fazLogout', () => {
    cy.get('a.logout').should('be.visible').and('contain', 'Sign out').click();
    cy.get('a.logout').should('not.exist');
    cy.get('a.login').should('be.visible').and('contain', 'Sign in');
});

Cypress.Commands.add('acessaCategoriaVestidosCasuais', () => {
    cy.contains('.sf-menu > li > a', 'Dresses' ).should('be.visible').and('contain' , 'Dresses').click();
    cy.contains('.tree > li > a', 'Casual Dresses' ).should('be.visible').and('contain' , 'Casual Dresses').click();
});

Cypress.Commands.add('adicionaProdutoAoCarrinho', () => {
    cy.contains('.button-container > a', 'Add to cart' ).should('be.visible').and('contain' , 'Add to cart').click();
    cy.get('.layer_cart_product > h2').should('be.visible')
        .and('contain' , 'Product successfully added to your shopping cart');
});

Cypress.Commands.add('acessaEVerificaPaginaResumoDoCarrinhoDeCompras', () => {
    cy.contains('.button-container > a > span', 'Proceed to checkout').should('be.visible')
            .and('contain', 'Proceed to checkout').click();
        cy.get('#cart_title').should('be.visible').and('contain', 'Shopping-cart summary').click();
});

Cypress.Commands.add('verificaPaginaDeEnderecosEProcessa', () => {
    cy.get('h1.page-heading').should('be.visible').and('contain', 'Addresses');
    cy.contains('button[name="processAddress"]', 'Proceed to checkout').should('be.visible')
        .and('contain', 'Proceed to checkout').click();
});

Cypress.Commands.add('verificaPaginaDeFreteCheckaOsTermosEProcessa', () => {
    cy.get('h1.page-heading').should('be.visible').and('contain', 'Shipping');
    cy.get('#cgv').should('be.not.checked').check().should('be.checked');
        cy.contains('button[name="processCarrier"]', 'Proceed to checkout').should('be.visible')
            .and('contain', 'Proceed to checkout').click();
});

Cypress.Commands.add('verificaPaginaDePagamentoEEscolheForma', () => {
    cy.get('h1.page-heading').should('be.visible').and('contain', 'Please choose your payment method');
    cy.get('.payment_module > .bankwire').should('be.visible')
            .and('contain', 'Pay by bank wire ').click();
});

Cypress.Commands.add('verificaPaginaDePedidoEConfirmaPedido', () => {
    cy.get('h1.page-heading').should('be.visible').and('contain', 'Order summary');
    cy.contains('button > span', 'I confirm my order').should('be.visible')
            .and('contain', 'I confirm my order').click();
});