# desafio-automacao
Projeto Desafio para criação de Plano de testes, Desenvolvimendo dos scripts de automação com Cypress e Geração de Relatorio com MochaWesome Reporter.

## Pré-Requisitos
É requerido ter Node.js e npm instalados para rodar o projeto.

> Usadas as versões `v16.15.0` e `8.5.5` do Node.js e npm, respectivamente.
## Instalação

Utilizar o comando `npm install`para instalação das dependencias.

## Testes

Utilizar o comando `npm run cypress:run` para rodar os testes em modo headless.

Ou, utilizar o comando `npm run cypress:open` para abrir o modo interativo do Cypress.


## Relatório

Ao utilizar o comando `npm run cypress:run` será gerado um arquivo chamado `index.html`.

O mesmo encontra-se dentro de cypress/reports/html. Execute o arquivo em um navegador para visualização do relatório.


